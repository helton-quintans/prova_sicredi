<a name="readme-top"></a>

<div align="center">
  <a href="https://github.com/helton-quintans/prova_sicredi"></a>

<h1 align="center">Prova Sicredi - QA</h1>

  <p align="center">
    Prova técnica de Automação de Teste
    <br />
    <a href="https://github.com/helton-quintans/prova_sicredi">
  </p>
  
  [![forthebadge made-with-java](http://ForTheBadge.com/images/badges/made-with-java.svg)]()
  [![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
  <br/>
  <br/>
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEZUXzB7kUbhsa4Hl5d6wDmGQFcvYUGy-6Ifr8Fj7vOPPqNDNxNzoW1_FrVfaurdc314I&usqp=CAU">
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Tabela de Conteúdos</summary>
  <ol>
    <li>
      <a href="#about-the-project">Sobre o Projeto</a>
        <li><a href="#built-with">Desafio 01</a></li>
      </ul>
      <ul>
        <li><a href="#built-with">Desafio 02</a></li>
      </ul>
      <ul>
        <li><a href="#built-with">Construido Com</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Inicializando o projeto</a>
      <ul>
        <li><a href="#installation">Instação</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contato</a></li>
    
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## Sobre o Projeto

Projeto baseado em Maven criado com o IntelliJ utilizando a linguagem de programação Java.

### Desafio 1:

Observação:
O script deve executar no browser Google Chrome

Passos:

1. Acessar a página:  https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap
2. Mudar o valor da combo Select version para “Bootstrap V4 Theme”
3. Clicar no botão Add Customer
4. Preencher os campos do formulário com as seguintes informações:
- Name: Teste Sicredi
- Last name: Teste
- ContactFirstName: seu nome
- Phone: 51 9999-9999
- AddressLine1: Av Assis Brasil, 3970
- AddressLine2: Torre D
- City: Porto Alegre
- State: RS
- PostalCode: 91000-000
- Country: Brasil
- from Employeer: Fixter
- CreditLimit: 200
5. Clicar no botão Save
6. Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção
7. Fechar o browser

### Desafio 2:

Observação:
O script deve executar no browser Google Chrome

Pré-condição:
Execute todos os passos do Desafio 1

Passos:

1. Clicar no link Go back to list
2. Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi)
3. Clicar no checkbox abaixo da palavra Actions
4. Clicar no botão Delete
5. Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada
6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”
7. Fechar o browser

Final do Desafio!

## Construído Com
* `IntelliJ`
* `Java`
* `Maven`
* `Selenium`
* `Cucumber`
* `JUnit`

<!-- GETTING STARTED -->
## Inicializando o projeto

1. Escolha sua IDE favorita
2. Clone o repositório
```sh
git clone https://github.com/helton_quintans/prova_sicredi.git
```
3. Execute o arquivo `RunCucumberTest` - `src/test/java/runner/RunCucumberTest.java`

<!-- CONTACT -->
## Contato


<img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" /> 
<a href="https://mail.google.com/mail/u/1/#inbox" >helton.quit@gmail.com</a>


<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" /> <a href="https://mail.google.com/mail/u/1/#inbox" >https://www.linkedin.com/in/heltonquintans/</a>


<p align="right">(<a href="http">back to top</a>)</p>




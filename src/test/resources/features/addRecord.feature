# language: en

@challenges
Feature: Save the data filled in the form
  Me as grocerycrud user
  I want to fill the data in the form
  to save to database

  @fill-save-form
  Scenario: Fill and save the form
    Given I am on the grocerycrud website
    When I select Bootstrap V4 Theme version
    And I click on the Add Record button
    And fill out the form
    And click the save button
    Then I see confirmation that the data has been successfully stored

  @delet-form
  Scenario: Delete form data
    Given that I click on the Go back to list link
    When search by name Teste Sicredi
    And I click on the delete button
    And I verify that the message presented in the popup is correct
    And I click again on the delete button displayed in the popup
    Then I see confirmation that the data was successfully deleted
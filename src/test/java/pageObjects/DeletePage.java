package pageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import support.Utils;

public class DeletePage  extends Utils {

    WebDriver driver;
    public DeletePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickGoBack() {
        driver.findElement(By.cssSelector("#report-success > p > a:nth-child(2)")).click();
    }

    public void searchItem() {
        driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]/input")).sendKeys("Teste Sicredi");
    }

    public void clickActions() {
        driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/tbody/tr[1]/td[1]/input")).click();
    }
    public void clickButtonDelete() {
        driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[2]/div[1]/a")).click();
    }

    public void validatePopupMessage() {
        waitforelements(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]"), 2000);
        Assert.assertEquals("Are you sure that you want to delete this 1 item?",true, driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]")).isDisplayed());
    }

    public void deleteData() {
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]")).click();
    }

    public void validateDeleteMessage() {
        waitforelements(By.xpath("/html/body/div[3]/span[3]/p"), 2000);
        Assert.assertEquals("Your data has been successfully deleted from the database.",true, driver.findElement(By.xpath("/html/body/div[3]/span[3]/p")).isDisplayed());
    }

}

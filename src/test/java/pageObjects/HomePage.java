package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    WebDriver driver;
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void accessApplication() {
        driver.manage().window().maximize();
        driver.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
    }

    public void changeTheme() {
        driver.findElement(By.id("switch-version-select")).click();
        driver.findElement(By.cssSelector("#switch-version-select > option:nth-child(4)")).click();
    }

    public void clickAddRecord() {
        driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();
    }
}

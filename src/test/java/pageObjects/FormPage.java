package pageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import support.Utils;

public class FormPage extends Utils {

    WebDriver driver;
    public FormPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillForm() {
        driver.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
        driver.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        driver.findElement(By.id("field-contactFirstName")).sendKeys("Helton");
        driver.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        driver.findElement(By.id("field-addressLine1")).sendKeys("Av Assis Brasil, 3970");
        driver.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        driver.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        driver.findElement(By.id("field-state")).sendKeys("RS");
        driver.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        driver.findElement(By.id("field-country")).sendKeys("Brasil");
        driver.findElement(By.id("field-salesRepEmployeeNumber")).sendKeys("999");
        driver.findElement(By.id("field-creditLimit")).sendKeys("200");
        driver.findElement(By.id("field-deleted")).sendKeys("false");
    }

    public void saveData() {
        driver.findElement(By.id("form-button-save")).click();
    }

    public void validateMessage() {
        waitforelements(By.cssSelector("#report-success > p"), 2000);
        Assert.assertEquals("Your data has been successfully stored into the database. ",true, driver.findElement(By.cssSelector("#report-success > p")).isDisplayed());
    }

}

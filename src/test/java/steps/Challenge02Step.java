package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.DeletePage;
import runner.RunCucumberTest;

public class Challenge02Step extends RunCucumberTest {
    DeletePage deletePage = new DeletePage(driver);

    @Given("^that I click on the Go back to list link$")
    public void that_I_click_on_the_Go_back_to_list_link() {
        deletePage.clickGoBack();
    }

    @When("^search by name Teste Sicredi$")
    public void search_by_name_Teste_Sicredi() {
        deletePage.searchItem();
    }

    @When("^I click on the delete button$")
    public void i_click_on_the_delete_button() {
        deletePage.clickActions();
        deletePage.clickButtonDelete();
    }

    @When("^I verify that the message presented in the popup is correct$")
    public void i_verify_that_the_message_presented_in_the_popup_is_correct() {
        deletePage.validatePopupMessage();
    }

    @When("^I click again on the delete button displayed in the popup$")
    public void i_click_again_on_the_delete_button_displayed_in_the_popup() {
        deletePage.deleteData();
    }

    @Then("^I see confirmation that the data was successfully deleted$")
    public void i_see_confirmation_that_the_data_was_successfully_deleted() {
        deletePage.validateDeleteMessage();
    }

}

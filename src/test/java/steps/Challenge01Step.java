package steps;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.FormPage;
import pageObjects.HomePage;
import runner.RunCucumberTest;


public class Challenge01Step extends RunCucumberTest {

    HomePage homePage = new HomePage(driver);
    FormPage formPage = new FormPage(driver);


    @Given("^I am on the grocerycrud website$")
    public void i_am_on_the_grocerycrud_website() {
        homePage.accessApplication();
    }

    @When("^I select Bootstrap V4 Theme version$")
    public void i_select_Bootstrap_V4_Theme_version() {
        homePage.changeTheme();
    }

    @When("^I click on the Add Record button$")
    public void i_click_on_the_Add_Record_button() {
        homePage.clickAddRecord();
    }

    @When("^fill out the form$")
    public void fill_out_the_form() {
        formPage.fillForm();
    }

    @When("^click the save button$")
    public void click_the_save_button() {
        formPage.saveData();
    }

    @Then("^I see confirmation that the data has been successfully stored$")
    public void i_see_confirmation_that_the_data_has_been_successfully_stored() {
        formPage.validateMessage();
    }
}
